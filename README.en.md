# map-collection

## 🌏 [English](./README.en.md) | [中文](./README.md)

#### Description
地图相关的各种框架（mapbox、maptalks、cesium等）的集合仓库

#### Desktop applications

+ [mapbox](./apps/mapbox-win-0.0.1-x64.exe)
+ [maptalks](./apps/maptalks-win-0.0.1-x64.exe)
+ [cesium](./apps/cesium-win-0.0.1-x64.exe)

#### source repository

+ [mapbox](https://gitee.com/bidding-M/mapbox-test)
+ [maptalks](https://gitee.com/bidding-M/maptalks-test-next)
+ [cesium](https://gitee.com/bidding-M/cesium-test-next)
+ [openlayers](https://gitee.com/bidding-M/openlayers-test)
