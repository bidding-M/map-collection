# map-collection

![](https://img.shields.io/badge/-mapbox-000000?style=flat-square&logo=mapbox&logoColor=FFFFFF)
![](https://img.shields.io/badge/-maptalks-396CB2?style=flat-square&logo=maplibre&logoColor=FFFFFF)
![](https://img.shields.io/badge/-cesium-6CADDF?style=flat-square&logo=cesium&logoColor=FFFFFF)
![](https://img.shields.io/badge/-openlayers-1F6B75?style=flat-square&logo=openlayers&logoColor=FFFFFF)

## 🌏 [English](./README.en.md) | [中文](./README.md)

#### 介绍

地图相关的各种框架（mapbox、maptalks、cesium等）的集合仓库

#### 桌面端安装包(x64)

+ [mapbox](./apps/mapbox-win-0.0.1-x64.exe)
+ [maptalks](./apps/maptalks-win-0.0.1-x64.exe)
+ [cesium](./apps/cesium-win-0.0.1-x64.exe)

#### 源仓库

+ [mapbox](https://gitee.com/bidding-M/mapbox-test)
+ [maptalks](https://gitee.com/bidding-M/maptalks-test-next)
+ [cesium](https://gitee.com/bidding-M/cesium-test-next)
+ [openlayers](https://gitee.com/bidding-M/openlayers-test)
